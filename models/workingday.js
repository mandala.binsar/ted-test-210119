"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class WorkingDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.User, {
        through: "UserWorkingDays",
        foreignKey: "workingDayId",
        as: "users",
      });
    }
  }
  WorkingDay.init(
    {
      weekDay: DataTypes.STRING,
      workingDate: DataTypes.DATE,
      isWorking: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "WorkingDay",
    }
  );
  return WorkingDay;
};
