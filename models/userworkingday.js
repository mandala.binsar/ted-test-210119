"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class UserWorkingDay extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserWorkingDay.init(
    {
      userId: DataTypes.INTEGER,
      workingDayId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "UserWorkingDay",
    }
  );
  return UserWorkingDay;
};
