const { Company, User, WorkingDay } = require("../models");

module.exports = {
  async getUserList(req, res) {
    try {
      const user = await User.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });
      res.status(200).send(user);
    } catch (error) {
      res.status(500).send(error);
    }
  },
  async getUserListWithWorkingDay(req, res) {
    try {
      const user = await User.findAll({
        include: [
          "company",
          {
            model: WorkingDay,
            as: "workingDays",
            attributes: ["id", "weekDay", "workingDate", "isWorking"],
            through: {
              attributes: [],
            },
          },
        ],
      });
      res.status(200).send(user);
    } catch (error) {
      res.status(500).send(error);
    }
  },
};
