const { Company, User } = require("../models");

module.exports = {
  async getCompanyList(req, res) {
    try {
      const company = await Company.findAll({
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      });
      res.status(200).send(company);
    } catch (error) {
      res.status(500).send(error);
    }
  },
  //to be implemented
  async addCompany(req, res) {
    try {
      const company = null;
      res.status(200).send(company);
    } catch (error) {
      res.status(500).send(error);
    }
  },
  //to be implemented
  async deleteCompany(req, res) {
    try {
      const company = null;
      res.status(200).send(company);
    } catch (error) {
      res.status(500).send(error);
    }
  },
  //to be implemented
  async updateCompany(req, res) {
    try {
      const company = null;
      res.status(200).send(company);
    } catch (error) {
      res.status(500).send(error);
    }
  },
  //still wrong
  async getCompanyUser(req, res) {
    try {
      const companyId = req.param;
      console.log(companyId);
      const company = await Company.findByPk(2, {
        include: [
          {
            model: User,
            as: "user",
          },
        ],
      });
      res.status(200).send(company);
    } catch (error) {
      res.status(500).send(error);
    }
  },
};
