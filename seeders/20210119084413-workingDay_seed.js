"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    return queryInterface.bulkInsert(
      "WorkingDays",
      [
        {
          weekDay: "Senin",
          workingDate: "2020-01-18",
          isWorking: true,
        },
        {
          weekDay: "Selasa",
          workingDate: "2020-01-19",
          isWorking: true,
        },
        {
          weekDay: "Rabu",
          workingDate: "2020-01-20",
          isWorking: true,
        },
        {
          weekDay: "Kamis",
          workingDate: "2020-01-21",
          isWorking: true,
        },
        {
          weekDay: "Jumat",
          workingDate: "2020-01-22",
          isWorking: true,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("WorkingDays", null, {});
  },
};
