"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Users",
      [
        {
          name: "Mandala",
          address: "Tangsel",
          email: "mandala@telkomsel.com",
          companyId: 1,
        },
        {
          name: "Deli",
          address: "Blitar",
          email: "deli@telkomsel.com",
          companyId: 1,
        },
        {
          name: "Someone",
          address: "Somewhere",
          email: "someone@telkom.com",
          companyId: 2,
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("Users", null, {});
  },
};
