var express = require("express");
var router = express.Router();

const userControllers = require("../controllers/userControllers");

/* GET users listing. */
router.get("/", userControllers.getUserList);
router.get(
  "/getUserListWithWorkingDay",
  userControllers.getUserListWithWorkingDay
);

module.exports = router;
