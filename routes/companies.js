var express = require("express");
var router = express.Router();

const companyControllers = require("../controllers/companyControllers");

/* GET home page. */
router.get("/", companyControllers.getCompanyList);
router.get("/user/:id", companyControllers.getCompanyUser);

module.exports = router;
